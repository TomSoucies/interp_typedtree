open Data

let stdlib_flag = [Open (Longident.Lident "Stdlib")]
(* let no_stdlib_flag = [] *)

let utils = List.map (Filename.concat "utils") [
  "config.ml";
  "misc.ml";
  "identifiable.ml";
  "numbers.ml";
  "arg_helper.ml";
  "clflags.ml";
  "tbl.ml";
  "profile.ml";
  "terminfo.ml";
  "ccomp.ml";
  "warnings.ml";
  "consistbl.ml";
  "strongly_connected_components.ml";
  "build_path_prefix_map.ml";
  "targetint.ml";
]

let parsing = List.map (Filename.concat "parsing") [
  "asttypes.mli";
  "location.ml";
  "longident.ml";
  "parsetree.mli";
  "docstrings.ml";
  "syntaxerr.ml";
  "ast_helper.ml";
  "parser.ml";
  "lexer.ml";
  "parse.ml";
  "printast.ml";
  "pprintast.ml";
  "ast_mapper.ml";
  "ast_iterator.ml";
  "attr_helper.ml";
  "builtin_attributes.ml";
  "ast_invariants.ml";
  "depend.ml";
]

let pure_typing = List.map (Filename.concat "typing") [
  "ident.ml";
  "outcometree.mli";
  "annot.mli";
  "path.ml";
  "primitive.ml";
  "types.ml";
  "btype.ml";
  "oprint.ml";
  "subst.ml";
  "predef.ml";
  "datarepr.ml";
  "cmi_format.ml";
  "env.ml";
  "typedtree.ml";
  "printtyped.ml";
  "ctype.ml";
  "printtyp.ml";
  "includeclass.ml";
  "mtype.ml";
  "envaux.ml";
  "includecore.ml";
  "typedtreeIter.ml";
  "typedtreeMap.ml";
  "tast_mapper.ml";
  "cmt_format.ml";
  "untypeast.ml";
  "includemod.ml";
  "typetexp.ml";
  "printpat.ml";
  "parmatch.ml";
  "stypes.ml";
  "typedecl.ml";
]

let lambda = List.map (Filename.concat "bytecomp") [
  "lambda.ml";
]

let more_typing = List.map (Filename.concat "typing") [
  "typeopt.ml";
  "typecore.ml";
  "typeclass.ml";
  "typemod.ml";
]

let bytecomp = List.map (Filename.concat "bytecomp") [
  "cmo_format.mli";
  "printlambda.ml";
  "semantics_of_primitives.ml";
  "switch.ml";
  "matching.ml";
  "translobj.ml";
  "translattribute.ml";
  "translprim.ml";
  "translcore.ml";
  "translclass.ml";
  "translmod.ml";
  "simplif.ml";
  "runtimedef.ml";
  "meta.ml";
  "opcodes.ml";
  "bytesections.ml";
  "dll.ml";
  "symtable.ml";
]

let driver = List.map (Filename.concat "driver") [
  "pparse.ml";
  "main_args.ml";
  "compenv.ml";
  "compmisc.ml";
  "compdynlink.mlno";
  "compplugin.ml";
  "makedepend.ml";
]

let middle_end = List.map (Filename.concat "middle_end") [
  "base_types/id_types.ml";
  "base_types/compilation_unit.ml";
  "base_types/set_of_closures_id.ml";
  "base_types/symbol.ml";
  "base_types/variable.ml";
  "base_types/closure_element.ml";
  "base_types/closure_id.ml";
  "base_types/var_within_closure.ml";
  "base_types/linkage_name.ml";
  "flambda_utils.ml";
  "simple_value_approx.ml";
  "debuginfo.ml";
]

let asmcomp = List.map (Filename.concat "asmcomp") [
  "cmx_format.mli";
  "clambda.ml";
  "export_info.ml";
  "compilenv.ml";
  "import_approx.ml";

  "debug/reg_with_debug_info.ml";
  "debug/reg_availability_set.ml";
  "debug/available_regs.ml";
 
  "x86_ast.mli";
  "x86_proc.ml";
  "x86_dsl.ml";
  "x86_gas.ml";

  "arch.ml";
  "cmm.ml";
  "reg.ml";
  "mach.ml";
  "proc.ml";

  "selectgen.ml";
  "spacetime_profiling.ml";
  "selection.ml";

  "closure.ml";
  "strmatch.ml";
  "cmmgen.ml";
  "linearize.ml";
  "branch_relaxation.ml";
  "emitaux.ml";
  "emit.ml";
  "comballoc.ml";
  "CSEgen.ml";
  "CSE.ml";
  "liveness.ml";
  "deadcode.ml";
  "split.ml";
  "spill.ml";
  "interf.ml";
  "coloring.ml";
  "reloadgen.ml";
  "reload.ml";
  "schedgen.ml";
  "scheduling.ml";
  "asmgen.ml";

  "asmlink.ml";
  "asmlibrarian.ml";
]

let bytegen = List.map (Filename.concat "bytecomp") [
  "instruct.ml";
  "bytegen.ml";
  "printinstr.ml";
  "emitcode.ml";
  "bytelink.ml";
  "bytelibrarian.ml";
  "bytepackager.ml";
]

let bytecode_main = List.map (Filename.concat "driver") [
  "errors.ml";
  "compile.ml";
  "main.ml";
]

let native_main = List.map (Filename.concat "driver") [
  "opterrors.ml";
  "optcompile.ml";
  "optmain.ml";
]

let bytecode_compiler_units () =
  let compiler_source_path = Conf.compiler_source_path () in
  let fullpath file = Filename.concat compiler_source_path file in
  List.map (fun modfile -> stdlib_flag, fullpath modfile)
  ( utils
  @ parsing
  @ pure_typing
  @ lambda
  @ more_typing
  @ bytecomp
  @ driver
  @ bytegen
  @ bytecode_main
  )

let native_compiler_units () =
  let compiler_source_path = Conf.compiler_source_path () in
  let fullpath file = Filename.concat compiler_source_path file in
  List.map (fun modfile -> stdlib_flag, fullpath modfile)
  ( utils
  @ parsing
  @ pure_typing
  @ lambda
  @ more_typing
  @ bytecomp
  @ driver
  @ middle_end
  @ asmcomp
  @ native_main
  )