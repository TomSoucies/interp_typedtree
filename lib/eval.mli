open Data

module T = Typedtree
module A = Asttypes

type prims = value_ Ptr.t SMap.t
val eval_implem : prims -> env -> T.implementation -> env
val eval_structure : prims -> env -> T.structure -> env
val apply : prims -> value -> (A.arg_label * value) list -> value
