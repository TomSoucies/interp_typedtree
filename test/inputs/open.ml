open Module

module type S = S
module M : S = M

let b = Module.f 1
let a = M.f M.x