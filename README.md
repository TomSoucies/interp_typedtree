# Interp_typedtree

## Interpret the OCaml typedtree

This project's purpose is to have an interpreter based on the OCaml typedtree.
We try to export the Camlboot project on the typdetree, see:
https://github.com/Ekdohibs/camlboot

## Results

This project uses a dune architecture.
So after building the project, you can test it by interpreting the OCaml stdlib 4.07 (version of Camlboot):

```bash
dune exec -- interp_typedtree -ocaml_stdlib -nostdlib -nopervasives
```

It will print the execution of the standard library of OCaml
You can do the same thing with the OCaml compiler using the command:

```bash
dune exec -- interp_typedtree -ocaml_compiler -nostdlib -nopervasives
```

## How to use it

You can also use the following command:

```bash
dune exec -- interp_typedtree <flags> foo/bar/toto.ml\n\
```